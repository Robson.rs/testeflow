import { start } from './oldProxy';
import { Proxy } from './proxy';

const express = require('express');

// Create Express Server
const app = express();

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// Configuration
const PORT = 6635;
const HOST = '10.0.3.6';
const API_SERVICE_URL = 'http://10.0.3.6:6637/';

// Proxy endpoints
app.use((req, res) => {
  if (req.method === 'OPTIONS') {
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(204);
    res.end();
    return;
  } else {
    try {
      const oldBody = { ...req.body };
      req.body = oldBody.body;

      req.headers['authorization'] = 'Basic YWRtaW46NUk2QTEyMzQ=';
      if (req.body && req.body.length) {
        req.headers['content-length'] = req.body.length;
      }

      //  se não tem a barra adiciona
      if (!oldBody.route.match(/^(\/|\\)/)) {
        oldBody.route = '/' + oldBody.route;
      }

      if (req.url.match(/^\/protheusV2/gim)) {
        req.url = API_SERVICE_URL + 'rest' + oldBody.route;
      } else if (req.url.match(/^\/protheus/gim) && req.method === 'POST') {
        req.url = API_SERVICE_URL + 'rest/fluigweb' + oldBody.route;
      }

      console.log(req.url);

      Proxy(req.url, req, res);
    } catch (erro) {
      console.log(erro);
      res.status(500).type('*/*').send(erro);
    }
  }
});

// use http with instance of express
var http = require('http').createServer(app);
http.listen(PORT, HOST, () => {
  console.log(`Starting Proxy at ${HOST}:${PORT}`);
});

// create socket instance with http
var io = require('socket.io')(http);

// add listener for new connection
io.on('connection', function (socket) {
  // this is socket for each user
  console.log('User connected', socket.id);
});

io.on('connect_error', (err) => {
  console.log(`connect_error due to ${err.message}`);
});

start();
