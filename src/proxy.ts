import express from 'express';
import { Method, AxiosResponse, AxiosRequestConfig } from 'axios';

export const ExpressToAxios = async (
  url: string,
  request: express.Request,
  response: express.Response
): Promise<AxiosResponse<any>> => {
  try {
    const axios = require('axios');

    const data: AxiosRequestConfig = {
      url: url,
      method: request.method as Method,
      headers: {
        Authorization: 'Basic YWRtaW46NUk2QTEyMzQ=',
      },
      maxBodyLength: Infinity,
      params: request.params,
      data: request.body,
    };

    if (request.body && request.body.length) {
      data.headers['content-length'] = request.body.length;
    }

    console.log(data);

    return axios.request(data);
  } catch (erro) {
    console.log(erro);
    response.status(500).type('*/*').send(erro);
  }
};

export const AxiosToExpress = async (
  axiosResponse: AxiosResponse,
  expressResponse: express.Response
): Promise<express.Response<any>> => {
  try {
    return expressResponse
      .status(axiosResponse.status)
      .header(axiosResponse.headers)
      .type('*/*')
      .send(axiosResponse.data);
  } catch (erro) {
    console.log(erro);
    expressResponse.status(500).type('*/*').send(erro);
  }
};

export const Proxy = async (
  url: string,
  request: express.Request,
  response: express.Response
) => {
  try {
    return AxiosToExpress(
      await ExpressToAxios(url, request, response),
      response
    );
  } catch (erro) {
    console.log(erro);
    response.status(500).type('*/*').send(erro);
  }
};
