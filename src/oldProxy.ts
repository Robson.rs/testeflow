export function start() {
  const http = require('http');
  const httpProxy = require('http-proxy');
  const fs = require('fs');

  const userProxy = httpProxy.createProxyServer({});

  userProxy.on('proxyReq', async function (proxyReq, req, res, options) {});

  let server = http.createServer(function (req, res) {
    // You can define here your custom logic to handle the request
    // and then proxy the request.
    try {
      let url = 'http://10.0.3.6:6637/';
      let body = '';
      console.log(`URL FINAL ${url}.`);
      console.log(`HEADERS ${JSON.stringify(req.rawHeaders)}.`);

      req.on('data', function (data) {
        body += data;
      });

      userProxy.web(req, res, {
        target: url,
      });

      userProxy.on('end', () => {
        console.log('BODY ' + body);
        body = '';
      });

      userProxy.on('error', function (err, req, res) {
        console.log(err);
        res.end('Error occurr' + err);
      });
    } catch (erro) {
      console.log(erro);
    }
  });

  console.log('subindo o server');
  server.listen(6636, () => {
    console.log('server starting https on port : ' + 6636);
  });
}
